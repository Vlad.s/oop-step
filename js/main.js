class BuildModal {
    constructor(content = '', nameElement = 'modal') {
        this.content = content;
        this.nameElement = nameElement;
        this.namePlugin = 'modal';
        this.html = `<div id="${this.nameElement}-${this.namePlugin}-modal" class="${this.namePlugin}-modal">
        <span id="${this.nameElement}-${this.namePlugin}-close" class="${this.namePlugin}-close">X</span>
        <div id="${this.nameElement}-${this.namePlugin}-content" class="${this.namePlugin}-content">${this.content}</div>
      </div>`;
    }

    _renderModal() {
        const container = document.createElement('div');
        container.id = `${this.nameElement}-${this.namePlugin}-container`;
        container.className = `${this.namePlugin}-container`;
        const overlay = document.createElement('div');
        overlay.id = `${this.nameElement}-${this.namePlugin}-overlay`;
        overlay.className = `${this.namePlugin}-overlay`;
        container.innerHTML = this.html;
        container.appendChild(overlay);
        document.body.appendChild(container);
    }
    ;

    _openModal() {
        this._renderModal();
        const btn = document.getElementById('createButton');
        btn.addEventListener('click', () => {
            document.getElementById(`${this.nameElement}-${this.namePlugin}-overlay`).style.display = "block";
            document.getElementById(`${this.nameElement}-${this.namePlugin}-modal`).style.display = "flex";
            document.getElementById('chooseDoctor').selectedIndex = 0;
        });
        this.chooseDoctor()

    }

    _closeModal() {
        document.getElementById(`${this.nameElement}-${this.namePlugin}-overlay`).style.display = "none";
        document.getElementById(`${this.nameElement}-${this.namePlugin}-modal`).style.display = "none";
        document.getElementById('form').innerHTML = '';

    }

    _closeEvents() {
        document.getElementById(`${this.nameElement}-${this.namePlugin}-close`).addEventListener("click", () => {
            this._closeModal();
        });
        document.getElementById(`${this.nameElement}-${this.namePlugin}-overlay`).addEventListener("click", () => {
            this._closeModal();
        });
        document.getElementById('btnCreate').addEventListener('click', () => {
            this.sendValue();
        });
    }

    chooseDoctor() {
        InputForm.fullName = {type: "text", class: 'input-form', id: 'fullName', placeholder: 'ФИО'};
        InputForm.visitDate = {type: "text", class: 'input-form', id: 'visitData', placeholder: 'Дата&nbspвизита'};
        InputForm.visitTarget = {type: "text", class: 'input-form', id: 'visitTarget', placeholder: 'Цель&nbspвизита'};
        InputForm.normalPressure = {
            type: "text",
            class: 'input-form',
            id: 'normalPressure',
            placeholder: 'Нормальное&nbspдавление'
        };
        InputForm.indexMass = {
            type: "text",
            class: 'input-form',
            id: 'indexMass',
            placeholder: 'Индекс&nbspмассы&nbspтела'
        };
        InputForm.previousDiseases = {
            type: "text",
            class: 'input-form',
            id: 'previousDiseases',
            placeholder: 'Перенесенные&nbspзаболевания&nbspсердечно-сосудистой&nbspсистемы'
        };
        InputForm.age = {type: "text", class: 'input-form', id: 'age', placeholder: 'Возраст'};
        InputForm.lastVisitDate = {
            type: "text",
            class: 'input-form',
            id: 'lastVisitDate',
            placeholder: 'Дата&nbspпоследнего&nbspвизита'
        };
        document.getElementById('chooseDoctor').addEventListener('change', (e) => {
            const list = document.getElementById('chooseDoctor');
            const currentDoctor = list.options[list.selectedIndex].value;
            if (currentDoctor === 'cardiologist') {
                const inputCardio = new InputFormCardio(InputForm.fullName, InputForm.visitTarget, InputForm.visitDate, InputForm.normalPressure, InputForm.indexMass, InputForm.previousDiseases, InputForm.age);
                inputCardio.addInput()
            } else if (currentDoctor === 'dentist') {
                const inputDentist = new InputFormDentist(InputForm.fullName, InputForm.visitTarget, InputForm.visitDate, InputForm.lastVisitDate);
                inputDentist.addInput()
            } else if (currentDoctor === 'therapist') {
                const inputTherapist = new InputFormTherapist(InputForm.fullName, InputForm.visitTarget, InputForm.visitDate, InputForm.age);
                inputTherapist.addInput()
            } else {
                form.innerHTML = ''
            }
        });
    }

    sendValue() {
        const failText = document.getElementById('failText');
        const limit = document.getElementById('limitSymbol');
        if (failText) {
            failText.parentNode.removeChild(failText);
        }
        if (limit) {
            limit.parentNode.removeChild(limit);
        }
        const forma = document.getElementById('form');
        let val = [];
        const list = document.getElementById('chooseDoctor');
        const currentDoctor = list.options[list.selectedIndex].value;
        const inputs = forma.getElementsByClassName('input-form');
        const textArea = inputs[inputs.length - 1].value;
        for (let i = 0; i < inputs.length - 1; i++) {
            if (inputs[i].value === '') {
                const fail = document.createElement('p');
                forma.appendChild(fail);
                fail.className = 'fail-text';
                fail.id = 'failText';
                fail.innerText = 'Все поля ввода должны быть заполнены';
                return false;
            } else if (inputs[inputs.length - 1].value.length > 400) {
                const limitSymbols = document.createElement('p');
                forma.appendChild(limitSymbols);
                limitSymbols.className = 'fail-text';
                limitSymbols.id = 'limitSymbol';
                limitSymbols.innerText = 'Не больше 400 символово';
                return false;
            } else {
                val.push(inputs[i].value);
            }
        }
        val.push(textArea);
        if (currentDoctor === 'cardiologist') {
            const visitCardio = new CardioVisit(val[0], val[1], val[2], val[3], val[4], val[5], val[6], val[7], currentDoctor);
            console.log(visitCardio);
            visitCardio.render()
        } else if (currentDoctor === 'dentist') {
            const visitDentist = new DentistVisit(val[0], val[1], val[2], val[3], val[4], currentDoctor);
            visitDentist.render()
        } else if (currentDoctor === 'therapist') {
            const visitTherapist = new TherapistVisit(val[0], val[1], val[2], val[3], val[4], currentDoctor);
            visitTherapist.render()
        }
        for (let i = 0; i < inputs.length - 1; i++) {
            if (inputs[i].value !== '') {
                this._closeModal();
            }
        }
    }


}

class StartCardMethod {
    constructor() {

    }

    cardEvent() {
        const card = document.getElementsByClassName('card');
        if (card.length > 0) {
            StartCardMethod.showMore();
            StartCardMethod.removeCard();
        }
    }

    static showMore() {
        const btnShow = document.getElementsByClassName('show-more');
        for (let i = 0; i < btnShow.length; i++) {
            btnShow[i].addEventListener('click', function () {
                const hiddenSpan = document.getElementsByClassName(this.dataset.target);
                for (let i = 0; i < hiddenSpan.length; i++) {
                    hiddenSpan[i].classList.toggle("hidden");
                }
            })
        }
    }

    static removeCard() {
        const cross = document.getElementsByClassName('cross');
        const card = document.getElementsByClassName('card');
        const wrapper = document.getElementById('cardsWrapper');
        for (let i = 0; i < cross.length; i++) {
            cross[i].addEventListener('click', function () {
                document.getElementById(this.dataset.target).parentNode.removeChild(document.getElementById(this.dataset.target));
                localStorage.removeItem(this.dataset.target);
                if (card.length === 0) {
                    wrapper.innerHTML += `<p id="noItem">Any item have been added</p>`
                }
            })
        }
    }


  getLocalStorage() {
        for (let i = 0; i < localStorage.length; ++i) {
            const wrapper = document.getElementById('cardsWrapper');
            wrapper.innerHTML += localStorage.getItem(localStorage.key(i));
        }
    }

    noItem() {
        const card = document.getElementsByClassName('card');
        if (card.length > 0) {
            document.getElementById('noItem').parentNode.removeChild(document.getElementById('noItem'));
        }
    }
}

class DoctorVisit {
    constructor(fullName, visitTarget, visitData, textArea, chooseDoctor) {
        this.fullName = fullName;
        this.visitTarget = visitTarget;
        this.visitData = visitData;
        this.textArea = textArea;
        this.chooseDoctor = chooseDoctor;
        this.html = `
                        <span class="cross">X</span>
                        <span>Name: ${this.fullName}</span>
                        <span>Doctor: ${this.chooseDoctor}</span>
                        <span>Visit target: ${this.visitTarget}</span>
                        <span>Data: ${this.visitData}</span> 
                        `;
    }

    render() {
        const wrapper = document.getElementById('cardsWrapper');
        const card = document.createElement('div');
        card.className = 'card';
        wrapper.appendChild(card);
        const btn = document.createElement("button");
        btn.className = 'show-more';
        btn.innerText = 'Show more';
        btn.addEventListener('click', () => {
            if (btn.textContent === 'Show more') {
                btn.innerText = 'Roll up';
            } else {
                btn.innerText = 'Show more';
            }
        });
        card.innerHTML = this.html;
        if (this.textArea === undefined) {
            return;
        } else {
            const textArea = document.createElement('span');
            card.appendChild(textArea);
            textArea.innerText = this.textArea;
        }
        card.appendChild(btn);
        const span = card.getElementsByTagName("span");
        DoctorVisit.createClass();
        card.id = DoctorVisit.createId();
        span[0].setAttribute('data-target', card.id);
        DoctorVisit.removeCard();
        localStorage.setItem(card.id, card.outerHTML);
        const noItem = document.getElementById('noItem');
        if (noItem) {
            noItem.parentNode.removeChild(noItem);
        }
        // function move(e) {
        //     card.style.transform = `translate(${e.clientX - card.mousePositionX}px,${e.clientY - card.mousePositionY}px)`;
        // }
        //
        // card.addEventListener('mousedown',(e)=>{
        //     if (card.style.transform) {
        //         const transforms = card.style.transform;
        //         const transformX = parseFloat(transforms.split('(')[1].split(',')[0]);
        //         const transformY = parseFloat(transforms.split('(')[1].split(',')[1]);
        //         card.mousePositionX = e.clientX - transformX;
        //         card.mousePositionY = e.clientY - transformY;
        //     } else {
        //         card.mousePositionX = e.clientX;
        //         card.mousePositionY = e.clientY;
        //     }
        //     card.addEventListener('mousemove',move);
        // });
        //
        // card.addEventListener('mouseup', e => {
        //     card.removeEventListener('mousemove',move);
        // });
    }

    static createClass() {
        const card = document.getElementsByClassName('card');
        const btn = document.getElementsByClassName('show-more');
        let randomClass = '';
        const length = 7;
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            randomClass += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        for (let i = 0; i < btn.length; i++) {
            btn[btn.length - 1].setAttribute('data-target', randomClass);
        }
        let span = '';
        for (let i = 0; i < card.length; i++) {
            span = card[i].getElementsByTagName("span");
        }
        for (let i = 3; i < span.length; i++) {
            span[i].className += randomClass;
            span[i].className += ' hidden';
        }
        btn[btn.length - 1].addEventListener('click', function () {
            const hiddenSpan = document.getElementsByClassName(this.dataset.target);
            for (let i = 0; i < hiddenSpan.length; i++) {
                hiddenSpan[i].classList.toggle("hidden");
            }
        })
    }

    static createId() {
        let randomId = '';
        const length = 7;
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            randomId += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return randomId;
    }

    static removeCard() {
        const wrapper = document.getElementById('cardsWrapper');
        const card = document.getElementsByClassName('card');
        const cross = document.getElementsByClassName('cross');
        cross[cross.length - 1].addEventListener('click', function () {
            document.getElementById(this.dataset.target).parentNode.removeChild(document.getElementById(this.dataset.target));
            localStorage.removeItem(this.dataset.target);
            if (card.length === 0) {
                wrapper.innerHTML += `<p id="noItem">Any item have been added</p>`
            }
        })
    }


}

class CardioVisit extends DoctorVisit {
    constructor(fullName, visitTarget, visitData, normalPressure, indexMass, previousDiseases, age, textArea, chooseDoctor) {
        super(fullName, visitTarget, visitData, textArea, chooseDoctor);
        this.normalPressure = normalPressure;
        this.indexMass = indexMass;
        this.previousDiseases = previousDiseases;
        this.age = age;
        this.html += `
                        <span >Normal pressure: ${this.normalPressure}</span>
                        <span >Index mass: ${this.indexMass}</span>
                        <span >Previous diseases: ${this.previousDiseases}</span>
                        <span>Age: ${this.age}</span>
                        `;
    }

}

class DentistVisit extends DoctorVisit {
    constructor(fullName, visitTarget, visitData, lastVisitDate, textArea, chooseDoctor) {
        super(fullName, visitTarget, visitData, textArea, chooseDoctor);
        this.lastVisitDate = lastVisitDate;
        this.html += `<span>Last visit data: ${this.lastVisitDate}</span>`;
    }
}

class TherapistVisit extends DoctorVisit {
    constructor(fullName, visitTarget, visitData, age, textArea, chooseDoctor) {
        super(fullName, visitTarget, visitData, textArea, chooseDoctor);
        this.age = age;
        this.html += `<span>Age: ${this.age}</span>`;
    }
}

class InputForm {
    constructor(fullName, visitTarget, visitData) {
        this.fullName = fullName;
        this.visitTarget = visitTarget;
        this.visitData = visitData;
        this.textArea = `<textarea class="text-area input-form" name="" id="textArea" cols="30" rows="10" placeholder="400 symbols max"></textarea>`;
        this.html = `
<input type=${this.fullName.type} class=${this.fullName.class} id=${this.fullName.id} placeholder=${this.fullName.placeholder}>
<input type=${this.visitTarget.type} class=${this.visitTarget.class} id=${this.visitTarget.id} placeholder=${this.visitTarget.placeholder}>
<input type=${this.visitData.type} class=${this.visitData.class} id=${this.visitData.id} placeholder=${this.visitData.placeholder}>
`;
    }

    addInput() {
        const form = document.getElementById('form');
        const list = document.getElementById('chooseDoctor');
        form.innerHTML = this.html + this.textArea;
    }

}

class InputFormCardio extends InputForm {
    constructor(fullName, visitTarget, visitData, normalPressure, indexMass, previousDiseases, age) {
        super(fullName, visitTarget, visitData);
        this.normalPressure = normalPressure;
        this.indexMass = indexMass;
        this.previousDiseases = previousDiseases;
        this.age = age;
        this.html += `
<input type=${this.normalPressure.type} class=${this.normalPressure.class} id=${this.normalPressure.id} placeholder=${this.normalPressure.placeholder}>
<input type=${this.indexMass.type} class=${this.indexMass.class} id=${this.indexMass.id} placeholder=${this.indexMass.placeholder}>
<input type=${this.previousDiseases.type} class=${this.previousDiseases.class} id=${this.previousDiseases.id} placeholder=${this.previousDiseases.placeholder}>
<input type=${this.age.type} class=${this.age.class} id=${this.age.id} placeholder=${this.age.placeholder}>
`;
    }

}

class InputFormDentist extends InputForm {
    constructor(fullName, visitTarget, visitData, lastVisitDate) {
        super(fullName, visitTarget, visitData);
        this.lastVisitDate = lastVisitDate;
        this.html += `
        <input type=${this.lastVisitDate.type} class=${this.lastVisitDate.class} id=${this.lastVisitDate.id} placeholder=${this.lastVisitDate.placeholder}>
        `;
    }

}

class InputFormTherapist extends InputForm {
    constructor(fullName, visitTarget, visitData, age) {
        super(fullName, visitTarget, visitData);
        this.age = age;
        this.html += `
                <input type=${this.age.type} class=${this.age.class} id=${this.age.id} placeholder=${this.age.placeholder}>
        `;
    }


}

const startMet = new StartCardMethod();
startMet.getLocalStorage();
startMet.cardEvent();
startMet.noItem();

const buildModal = new BuildModal(`<select id=chooseDoctor name="chosenDoctor" class="choose-doctor" size="1">
            <option value="not-chosen">не выбрано</option>
            <option value="cardiologist" class="option">Cardiologist</option>
            <option value="dentist" class="option">Dentist</option>
            <option value="therapist" class="option">Therapist</option>
            </select>
            <form id="form" class="form-doctor">
           </form>
            <a href="#" class="create-button" id="btnCreate">create button</a>`, 'Modal');
buildModal._openModal();
buildModal._closeEvents();
